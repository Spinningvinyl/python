# Base project format.
# Documentation https://github.com/raspberrypilearning/getting-started-with-minecraft-pi/blob/master/worksheet.md
from mcpi.minecraft import Minecraft
from mcpi import block
import math

ip = "10.183.3.67"


def bottom(mc):
	

	mc.setBlocks(5,-3,5-z, 5,1,5-z, 5)
	mc.setBlocks(10,-3,5-z, 10,1,5-z, 5)
	mc.setBlocks(9,-3,5-z, 6,1,5-z, 4)

	mc.setBlocks(4,-3,4-z, 4,1,4-z, 5)
	mc.setBlocks(4,-3,3-z, 4,1,-1-z, 4)
	mc.setBlocks(4,-3,-1-z, 4,1,-1-z, 5)

	mc.setBlocks(5,-3,-2-z, 5,1,-2-z, 5)
	mc.setBlocks(6,-3,-2-z, 9,1,-2-z, 4)
	mc.setBlocks(10,-3,-2-z, 10,1,-2-z, 5)

	mc.setBlocks(11,-3,-1-z, 11,1,4-z, 5)
	mc.setBlocks(11,-3,0-z, 11,1,3-z, 4)

	mc.setBlocks(10,2,-1-z, 5,9,4-z, 17)

	mc.setBlocks(9,2,-1-z, 6,9,-1-z, 45)
	mc.setBlocks(5,2,0-z, 5,9,3-z, 45)
	mc.setBlocks(9,2,4-z, 6,9,4-z, 45)
	mc.setBlocks(10,2,0-z, 10,9,3-z, 45)

	mc.setBlocks(9,10,0-z, 9,17,3-z, 98)
	mc.setBlocks(9,10,3-z, 6,17,3-z, 98)
	mc.setBlocks(6,10,0-z, 6,17,3-z, 98)
	mc.setBlocks(9,9,0-z, 6,17,0-z, 98)

	mc.setBlocks(9,18,1-z, 11,18,2-z, 42)

	mc.setBlocks(12,18,1-z, 12,17,2-z, 5)

	mc.setBlocks(12,25,1-z, 12,19,1-z, 17)
	mc.setBlocks(12,17,-6-z, 12,17,0-z, 17)
	mc.setBlocks(12,18,3-z, 12,18,10-z, 17)
	mc.setBlocks(12,16,2-z, 12,10,2-z, 17)
def windmill(mc):
	mc.setBlocks(12,19,2-z, 12,19,7-z, 35)
	mc.setBlocks(12,20,2-z, 12,20,5-z, 35)
	mc.setBlocks(12,21,2-z, 12,21,4-z, 35)
	mc.setBlocks(12,22,2-z, 12,22,3-z, 35)
	mc.setBlocks(12,22,2-z, 12,24,2-z, 35)

	mc.setBlocks(12,18,0-z, 12,18,-5-z, 35)
	mc.setBlocks(12,19,0-z, 12,19,-3-z, 35)
	mc.setBlocks(12,20,0-z, 12,20,-2-z, 35)
	mc.setBlocks(12,21,0-z, 12,21,-1-z, 35)
	mc.setBlocks(12,22,0-z, 12,23,0-z, 35)

	mc.setBlocks(12,17,3-z, 12,17,8-z, 35)
	mc.setBlocks(12,16,3-z, 12,16,6-z, 35)
	mc.setBlocks(12,15,3-z, 12,15,5-z, 35)
	mc.setBlocks(12,14,3-z, 12,14,4-z, 35)
	mc.setBlocks(12,13,3-z, 12,12,3-z, 35)

	mc.setBlocks(12,16,1-z, 12,16,-4-z, 35)
	mc.setBlocks(12,15,1-z, 12,15,-2-z, 35)
	mc.setBlocks(12,14,1-z, 12,14,-1-z, 35)
	mc.setBlocks(12,13,1-z, 12,13,0-z, 35)
	mc.setBlocks(12,12,1-z, 12,11,1-z, 35)
def roof(mc):
	mc.setBlocks(9,18,3-z, 6,18,3-z, 53,3)
	mc.setBlocks(9,18,0-z, 6,18,0-z, 53,2)
	mc.setBlocks(6,18,2-z, 6,18,1-z, 53,0)  
	mc.setBlocks(9,19,2-z, 9,19,1-z, 53,1)

	mc.setBlocks(8,19,2-z, 7,19,1-z, 5)




if __name__ == "__main__":
	mc = Minecraft.create(ip, 4711)
	mc.player.setPos(0,0,0)
	mc.setBlocks(-128,-3,-128,128,64,128,0)
	mc.player.setPos(0,100  ,0)
	z = 0
	for i in range(0,10):
		bottom(mc)
		roof(mc)
		windmill(mc)
		z = z + 15




#45 brick
#98 stone brick
#42 is iron block
#35 is for wool
#wood for windmill is 7 each

mc.setBlock(1,0,0, 1,0,0, 103)# is x
mc.setBlock(0,0,1, 0,0,1, 246)#this is z
