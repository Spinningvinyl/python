import turtle
# main function
def main():
	w = turtle.Screen()
	w.clear()
	w.bgcolor("#00FFD3")
	screen = turtle.Screen()
	t = turtle.Turtle()
	t.hideturtle()
	#turtle.tracer(0, 0)
	
	
	t.penup()
	t.goto(-480,400)
	t.pendown()
	t.pencolor("#000000")#black
	t.fillcolor("#000000")#black
	t.begin_fill()
	t.width(10)
	t.fd(950)
	t.rt(90)
	t.fd(795)
	t.rt(90)
	t.fd(945)
	t.rt(90)
	t.fd(795)
	t.end_fill()
	t.penup()
	
	t.goto(-280,-260)
	t.pendown()
	t.pencolor("#ffffff")#white
	t.fillcolor("#F96302")#home depot orange
	t.begin_fill()
	t.width(10)
	t.fd(550)
	t.rt(90)
	t.fd(550)
	t.rt(90)
	t.fd(550)
	t.rt(90)
	t.fd(550)
	t.end_fill()
	t.penup()
	
	#T
	t.goto(-180,40)
	t.pendown()
	t.width(30)
	t.rt(55)
	t.fd(80)
	t.rt(85)
	t.fd(50)
	t.bk(100)
	t.penup()
	
	#H
	t.goto(-110,100)
	t.pendown()
	t.width(30)
	t.lt(85)
	t.fd(80)
	t.penup()
	t.goto(-120,145)
	t.pendown()
	t.rt(85)
	t.fd(50)
	t.penup()
	t.goto(-55,145)
	t.lt(85)
	t.pendown()
	t.fd(80)
	t.penup()
	
	#E
	t.goto(-10, 170)
	t.pendown()
	t.fd(80)
	t.rt(85)
	t.fd(50)
	t.penup()
	t.goto(-5, 175)
	t.pendown()
	t.fd(50)
	t.penup()
	t.goto(-30, 207)
	t.pendown()
	t.fd(50)
	t.penup()
	
	#H
	t.goto(-160,-180)
	t.pendown()
	t.width(40)
	t.lt(90)
	t.fd(120)
	t.penup()
	t.goto(-180,-120)
	t.pendown()
	t.rt(90)
	t.fd(50)
	t.lt(90)
	t.fd(60)
	t.bk(120)
	t.penup()
	
	#O
	t.goto(-50,-25)
	t.pendown()
	t.dot(155)
	t.pencolor("#F96302")#home depot orange
	t.dot(90)
	t.penup()
	t.pencolor("#ffffff")#white
	
	#M
	t.goto(75,0)
	t.pendown()
	t.fd(135)
	t.rt(120)
	t.fd(50)
	t.lt(65)
	t.fd(50)
	t.rt(125)
	t.fd(135)
	t.penup()
	
	#E
	t.goto(200,90)
	t.pendown()
	t.rt(180)
	t.fd(120)
	t.rt(90)
	t.fd(50)
	t.penup()
	t.goto(165,135)
	t.pendown()
	t.fd(50)
	t.penup()
	t.goto(205,95)
	t.pendown()
	t.fd(50)
	t.penup()
	
	#D
	t.goto(-70,-230)
	t.width(25)
	t.pendown()
	t.lt(90)
	t.fd(50)
	t.lt(90)
	t.circle(30, -180)
	t.penup()
	t.rt(90)
	t.rt(90)
	t.rt(90)
	
	#E
	t.goto(-13, -188)
	t.pendown()
	t.fd(50)
	t.rt(85)
	t.fd(25)
	t.penup()
	t.goto(-10, -190)
	t.pendown()
	t.fd(25)
	t.penup()
	t.goto(-26	, -168)
	t.pendown()
	t.fd(25)
	t.penup()
	
	#p
	t.goto(40, -140)
	t.pendown()
	t.lt(90)
	t.fd(50)
	t.lt(90)
	t.goto(13,-90)
	t.width(20)
	t.circle(16, -180)
	t.penup()
	t.width(30)
	
	#o
	t.goto(60,-70)
	t.pendown()
	t.dot(100)
	t.pencolor("#F96302")#home depot orange
	t.dot(35)
	t.penup()
	t.pencolor("#ffffff")#white
	
	
	
	
	#t.penup()
	#t.goto(0,180)
	#style = ('Stencil Antiqua AI-Reg ', 30, 'bold')
	#pyturtle.rotate(180)
	#t.write('THE HOME DEPOT', font=style, align='normal')
	#t.penup()
	
	
	
	screen.exitonclick()	
	

if __name__ == "__main__":
		main()
#lt = left turn
#rt = right turn
#bk = back
