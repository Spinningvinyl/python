import turtle
# main function
def main():
	w = turtle.Screen()
	w.clear()
	w.bgcolor("#00FFD3")
	screen = turtle.Screen()
	t = turtle.Turtle()
	t.hideturtle()
	#turtle.tracer(0, 0)
	t.speed(0)
	
	t.penup()
	t.goto(0,0)
	t.pendown()
	t.pencolor("#005fff")#blue
	t.begin_fill()
	t.width(10)
	t.dot(600)
	t.end_fill()
	t.penup()
	
	#white
	t.penup
	t.goto(-50,130)
	t.pendown()
	t.pencolor("#ffffff")#white
	t.fillcolor("#ffffff")#white
	t.begin_fill()
	t.width(10)
	t.dot(600)
	t.goto(-120,130)
	t.dot(600)
	t.goto(-150,130)
	t.dot(600)
	t.end_fill()
	t.penup()
	
	#red
	t.goto(-200, 280)
	t.pendown()
	t.pencolor("#ff2300")#white
	t.width(10)
	t.dot(800)
	t.penup()
	
	#circle
	#t.goto(0,-300)
	#t.pencolor("#000000")#black
	#t.pendown()
	#t.circle(300, 360)
	#t.penup()
	
	#background
	t.goto(0,300)
	t.pendown()
	t.begin_fill()
	t.pencolor("#ffffff")#black
	t.lt(180)
	t.circle(300,180)
	t.rt(90)
	t.fd(200)
	t.rt(90)
	t.fd(950)
	t.rt(90)
	t.fd(1000)
	t.rt(90)
	t.fd(950)
	t.rt(90)
	t.fd(200)
	t.end_fill()
	t.penup()
	
	#background
	t.goto(0,-300)
	t.pendown()
	t.begin_fill()
	t.pencolor("#ffffff")#black
	t.lt(90)
	t.circle(300,180)
	t.rt(90)
	t.fd(200)
	t.rt(90)
	t.fd(950)
	t.rt(90)
	t.fd(1000)
	t.rt(90)
	t.fd(950)
	t.rt(90)
	t.fd(200)
	t.end_fill()
	t.penup()
	
	#circle
	t.goto(0,-300)
	t.pencolor("#000000")#black
	t.pendown()
	t.rt(90)
	t.circle(300, 360)
	t.penup()
	
	#pepsi
	t.goto(0,-350)
	style = ('Sans-serif ', 30, 'bold')
	t.write('PEPSI', font=style, align='center')
	t.penup()
	
	screen.exitonclick()	
	
if __name__ == "__main__":
		main()
#lt = left turn
#rt = right turn
#bk = back
